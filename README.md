# ⇅ Internet Speed Meter

A simple and minimal internet speed meter extension for Gnome Shell.

## Table of Contents

[[_TOC_]]

## Screenshot

![Screenshot](screenshot.png)

## Getting Started

To use this extension, you will need

- Gnome Shell 3.20 or later

### Prerequisite:

#### Install Gnome Tweaks

For Ubuntu,

```bash
sudo add-apt-repository universe
sudo apt install gnome-tweaks
```

For Fedora,

```bash
sudo dnf install gnome-tweaks
```

For Arch Linux,

```bash
sudo pacman -S gnome-tweak-tool
```

You may find Gnome Tweaks for other distributions as well. Install them accordingly.

### Install Internet Speed Meter from Gnome Extensions

Visit [Internet Speed Meter](https://extensions.gnome.org/extension/2980/internet-speed-meter/) 

### Install Internet Speed Meter from source

1. Clone this repository

   ```bash
   git clone https://gitlab.com/AlShakib/InternetSpeedMeter.git
   ```

2. Change current directory to repository

   ```bash
   cd InternetSpeedMeter
   ```

3. Now run

   ```bash
   chmod +x ./install.sh && ./install.sh
   ```

4. Hit `ALT + F2`

5. Type `r` and hit `Enter`

6. Enable Internet Speed Meter extension in Gnome Tweaks

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Thanks to

- Project Icon is made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com)

## License

[GNU General Public License v3.0](LICENSE)

Copyright © 2020 [Al Shakib](https://alshakib.dev)
